import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    FlatList,
    ScrollView
} from 'react-native';

import { BackHeader } from '../components/Headers';
import { BizKimizItem } from '../components/BizKimizItems';
import * as colors from '../helpers/CollorPalette';

import styles from './styles';

import { bizKimiz } from '../data/data';

class BizKimiz extends Component {
    render() {
        return (
            <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: colors.white }} >
                <BackHeader titleFirst='Biz' titleSecond='Kimiz?' />
                <ScrollView showsVerticalScrollIndicator={false} style={[styles.neYaptikScrollViewContainerStyle, { backgroundColor: colors.white }]}>
                    <View>
                        <FlatList
                            data={bizKimiz}
                            renderItem={({item}) => <BizKimizItem title={item.title} description={item.description} />}
                            keyExtractor={(item) => item.id}
                        />
                    </View>
                    <View style={{ height: 40, backgroundColor: colors.white }}  />
                </ScrollView>
            </View>
        );
    }
}

export default BizKimiz;