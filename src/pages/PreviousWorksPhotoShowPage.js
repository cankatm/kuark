import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    Image,
    Dimensions,
    TouchableOpacity
} from 'react-native';
import ImageZoom from 'react-native-image-pan-zoom';

import * as colors from '../helpers/CollorPalette';
import * as fonts from '../helpers/fonts';
import styles from './styles';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

class PreviousWorksPhotoShowPage extends Component {
    state = {
        showSpinner: true
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.darkBlueKuark }} >
                <View style={styles.previousWorkBackHeaderContainerStyle} >
                    <TouchableOpacity activeOpacity={0.6} onPress={() => this.props.navigation.goBack()} >
                        <View style={styles.previousWorkBackHeaderButtonContainerStyle} >
                            <Text style={styles.previousWorkBackHeaderButtonXStyle}>X</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <ImageZoom
                cropWidth={Dimensions.get('window').width}
                cropHeight={Dimensions.get('window').height}
                imageWidth={WINDOW_WIDTH}
                imageHeight={WINDOW_HEIGHT}
                >
                    <Image
                        style={{ width: WINDOW_WIDTH, height: WINDOW_HEIGHT, resizeMode: 'contain' }}
                        source={this.props.navigation.state.params.imageSource}
                    />
                </ImageZoom>
                
                <View style={{ width: WINDOW_WIDTH, alignItems: 'flex-end', justifyContent: 'flex-end', position: 'absolute', bottom: 16, right: 16 }} >
                    <Text style={{ color: colors.white, fontFamily: fonts.msLight }}>{this.props.navigation.state.params.workName}</Text>
                </View>
            </View>
        );
    }
}

export default PreviousWorksPhotoShowPage;