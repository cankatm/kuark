import OpeningScreen from './OpeningScreen';
import MainPage from './MainPage';
import MenuPage from './MenuPage';
import BizKimiz from './BizKimiz';
import NeYapiyoruz from './NeYapiyoruz';
import NeYaptik from './NeYaptik';
import PreviousWorksPhotoShowPage from './PreviousWorksPhotoShowPage';
import BizeUlasin from './BizeUlasin';
import styles from './styles';

export { 
    OpeningScreen, 
    MainPage, 
    MenuPage, 
    BizKimiz, 
    NeYapiyoruz, 
    NeYaptik, 
    PreviousWorksPhotoShowPage, 
    BizeUlasin, 
    styles 
};