import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';

import * as colors from '../helpers/CollorPalette';
import * as fonts from '../helpers/fonts';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

export default StyleSheet.create({
    openingScreenContainerStyle: {
        width: WINDOW_WIDTH,
        height: WINDOW_HEIGHT,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.black
    },
    openingScreenImageStyle: { 
        width: 120, 
        height: 120, 
        resizeMode: 'cover' 
    },
    openingScreenTextStyle: {
        color: colors.white,
        fontSize: 14,
        fontFamily: fonts.msRegular
    },
    mainPageContainerStyle: {
        width: WINDOW_WIDTH,
        height: WINDOW_HEIGHT,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'rgba(0, 0, 0, 0.7)'
    },
    mainPageBackgroundImageStyle: {
        width: WINDOW_WIDTH,
        height: WINDOW_HEIGHT,
    },
    mainPageItemsContainerStyle: { 
        alignItems: 'center', 
        justifyContent: 'center', 
        flex: 1,
    },
    mainPageLogoContainerStyle: {
        marginTop: 24,
        width: WINDOW_WIDTH,
        alignItems: 'center',
        justifyContent: 'center',
    },
    mainPageLogoStyle: {
        width: 120,
        height: 120,
        resizeMode: 'contain'
    },
    mainPageBrandContainerStyle: {
        width: WINDOW_WIDTH,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    mainPageInformationContainerStyle: {
        marginTop: 8,
        width: WINDOW_WIDTH - 64,
        alignItems: 'center',
        justifyContent: 'center',
    },
    mainPageInformationTextStyle: {
        color: colors.white,
        textAlign: 'center',
        fontFamily: fonts.msRegular
    },
    mainPageButtonContainerStyle: {
        marginTop: 36,
        width: WINDOW_WIDTH - 64,
        backgroundColor: colors.blueKuark,
        height: 60,
        alignItems: 'center',
        justifyContent: 'center'
    },
    mainPageButtonTextStyle: {
        color: colors.white,
        fontSize: 18,
        fontFamily: fonts.msRegular
    },
    mainPageSocialMediaContainerStyle: { 
        width: WINDOW_WIDTH, 
        height: 50, 
        flexDirection: 'row', 
        justifyContent: 'space-between'
    },
    mainPageSocialMediaIconContainerStyle: { 
        width: 50, 
        height: 50, 
        alignItems: 'center',
        justifyContent: 'center'
    },
    neYaptikScrollViewContainerStyle: { 
        backgroundColor: colors.darkBlueKuark,
        height: WINDOW_HEIGHT - 170
    },
    neYaptikContainerStyle: { 
        width: WINDOW_WIDTH, 
        flexDirection: 'row', 
        flexWrap: 'wrap', 
        backgroundColor: colors.white 
    },
    previousWorkBackHeaderContainerStyle: {
        width: WINDOW_WIDTH,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        zIndex: 12
    },
    previousWorkBackHeaderButtonContainerStyle: {
        width: 40,
        height: 100,
        backgroundColor: colors.blueKuark,
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 12
    },
    previousWorkBackHeaderButtonXStyle: {
        color: colors.white,
        fontSize: 24
    },
});