import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    TouchableOpacity,
    Image
} from 'react-native';

import styles from './styles';

import kuarkLogo from '../../assets/images/kuarkLogo.png';

class OpeningScreen extends Component {
    state= {
        loading: true,
        counter: 0,
        roles: ['Web Design', 'Mobile Apps', 'Creative', 'Social Media', 'Adwords', 'SEO'],
    }

    componentWillMount() {
        setInterval(() => {
          this.setState({ counter: this.state.counter + 1 });
          if (this.state.counter >= this.state.roles.length) {
            this.setState({ counter: 0 });
          }
        }, 200);
        
        setTimeout(() => this.props.navigation.navigate('MainPage'), 1300);
    }

    render() {
        return (
            <View style={styles.openingScreenContainerStyle} >
                <Image source={kuarkLogo} style={styles.openingScreenImageStyle} />
                <Text style={styles.openingScreenTextStyle} >{this.state.roles[this.state.counter]}</Text>
            </View>
        );
    }
}

export default OpeningScreen;