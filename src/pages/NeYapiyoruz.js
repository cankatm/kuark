import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    TouchableOpacity,
    FlatList,
    ScrollView,
    Dimensions
} from 'react-native';

import { BackHeader } from '../components/Headers';
import { NeYapiyoruzButton } from '../components/NeYapiyoruzItems';
import * as colors from '../helpers/CollorPalette';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

import { neYapiyoruzItems } from '../data/data';
import styles from './styles';

class NeYapiyoruz extends Component {
    render() {
        return (
            <View>
                <BackHeader titleFirst='Ne' titleSecond='Yapıyoruz?' />
                <ScrollView 
                    style={{ height: WINDOW_HEIGHT, backgroundColor: colors.white }} 
                    showsVerticalScrollIndicator={false}
                >
                    <FlatList
                        data={neYapiyoruzItems}
                        renderItem={({item}) => (
                            <NeYapiyoruzButton iconName={item.iconName} title={item.title} description={item.description} id={item.id} />
                        )}
                        keyExtractor={(item) => item.id}
                    />
                    <View style={{ height: 170, width: WINDOW_WIDTH }} />
                </ScrollView>
            </View>
        );
    }
}

export default NeYapiyoruz;