import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar } from 'react-native';

import { MenuItem } from '../components/MenuItems';

import backgroundImage1 from '../../assets/images/menuuc.jpg';
import backgroundImage2 from '../../assets/images/menubir.jpg';
import backgroundImage3 from '../../assets/images/menuiki.jpg';
import backgroundImage4 from '../../assets/images/menudort.jpg';

import styles from './styles';

class MenuPage extends Component {
    render() {
        return (
            <View>
                <MenuItem title='Biz Kimiz?' bgImage={backgroundImage1} navigationTo='BizKimiz' />
                <MenuItem title='Ne Yapıyoruz?' bgImage={backgroundImage2} navigationTo='NeYapiyoruz' />
                <MenuItem title='Ne Yaptık?' bgImage={backgroundImage3} navigationTo='NeYaptik' />
                <MenuItem title='Bize Ulaşın' bgImage={backgroundImage4} navigationTo='BizeUlasin' />
            </View>
        );
    }
}
export default MenuPage;