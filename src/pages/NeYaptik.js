import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    Dimensions,
    ScrollView,
    FlatList
} from 'react-native';

import { images } from '../data/data';

import { BackHeader } from '../components/Headers';
import { ImageShowArea } from '../components/ImageShowAreas';

import styles from './styles';

class NeYaptik extends Component {
    render() {
        return (
            <View>
                <BackHeader titleFirst='Ne' titleSecond='Yaptık?' />
                <ScrollView showsVerticalScrollIndicator={false} style={styles.neYaptikScrollViewContainerStyle}>
                    <View style={styles.neYaptikContainerStyle} >
                        <FlatList
                            data={images}
                            renderItem={({item}) => <ImageShowArea imageSource={item.imageSource} workName={item.workName} />}
                            keyExtractor={(item) => item.workName}
                            style={{flexDirection: 'column'}}
                            numColumns={2}
                        />
                    </View>
                </ScrollView>
            </View>
        );
    }
}

export default NeYaptik;