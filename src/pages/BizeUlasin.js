import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    Image,
    ScrollView
} from 'react-native';
import Communications from 'react-native-communications';

import { BackHeader } from '../components/Headers';
import { BizeUlasinItem } from '../components/BizeUlasinItems';
import { googleAddress, phoneNumber, emailAddress } from '../data/data';

import bgImage from '../../assets/images/34.jpg';

import styles from './styles';

class BizeUlasin extends Component {
    render() {
        return (
            <View >
                <BackHeader titleFirst='Bize' titleSecond='Ulaşın' />
                <ScrollView showsVerticalScrollIndicator={false} >
                    <BizeUlasinItem 
                        communication={true} 
                        url={googleAddress}
                        iconName='ios-map-outline'
                        title='Adres'
                        contentFirst='Reşit Galip Caddesi No:45/8'
                        contentSecond='GOP Çankaya ANKARA'
                    />
                    <BizeUlasinItem 
                        onPress={() => Communications.phonecall(phoneNumber, true)} 
                        iconName='ios-call-outline'
                        title='Telefon'
                        contentFirst='+90 312 417 77 93'
                        contentSecond='+90 539 607 06 04'
                    />
                    <BizeUlasinItem 
                        onPress={() => Communications.email([emailAddress], null, null, null, '')} 
                        iconName='ios-mail-outline'
                        title='Email'
                        contentFirst='info@kuarktek.com.tr'
                        contentSecond={null}
                    />
                    <View style={{ height: 190, width: 20 }} />
                    <Image source={bgImage} style={{ width: 100, height: 100, resizeMode: 'contain' }} />
                </ScrollView>
            </View>
        );
    }
}

export default BizeUlasin;