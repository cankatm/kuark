import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    Image,
    ImageBackground,
    TouchableOpacity,
    Alert,
    Linking
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import styles from './styles';
import * as colors from '../helpers/CollorPalette';
import * as fonts from '../helpers/fonts';

import { kuarkLogo } from '../data/data';

import backgroundImage1 from '../../assets/images/menuuc.jpg';
import backgroundImage2 from '../../assets/images/menubir.jpg';
import backgroundImage3 from '../../assets/images/menuiki.jpg';
import backgroundImage4 from '../../assets/images/menudort.jpg';
const imageArray=[backgroundImage1, backgroundImage2, backgroundImage3, backgroundImage4];

class MainPage extends Component {
    constructor(props) {
        super(props);
        this.state={
            counter: 0,
            imageArray: [backgroundImage1, backgroundImage2, backgroundImage3, backgroundImage4]
        }
    }

    componentWillMount() {
        setInterval(() => {
            this.setState({ counter: this.state.counter + 1 });
            if (this.state.counter > this.state.imageArray.length) {
                this.setState({ counter: 0 });
            }
        }, 2500);
    }

    render() {
        const handlePressFacebook = () => {
            Alert.alert(
              'Facebook',
              'Uygulamadan ayrılıp Facebook\'u açmak üzeresiniz, devam etmek için \'Devam\' tuşuna basın',
              [
                {text: 'Devam', onPress: () => Linking.openURL('https://www.facebook.com/kuarktek/')},
                {text: 'İptal', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
              ]
            );
        }
        
        const handlePressTwitter = () => {
            Alert.alert(
              'Twitter',
              'Uygulamadan ayrılıp Twitter\'ı açmak üzeresiniz, devam etmek için \'Devam\' tuşuna basın',
              [
                {text: 'Devam', onPress: () => Linking.openURL('https://twitter.com/kuarktek')},
                {text: 'İptal', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
              ]
            );
        }
        const handlePressInstagram = () => {
            Alert.alert(
              'Instagram',
              'Uygulamadan ayrılıp Instagram\'ı açmak üzeresiniz, devam etmek için \'Devam\' tuşuna basın',
              [
                {text: 'Devam', onPress: () => Linking.openURL('https://www.instagram.com/kuarktek/')},
                {text: 'İptal', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
              ]
            );
        }

        return (
            <ImageBackground style={styles.mainPageBackgroundImageStyle} source={this.state.imageArray[this.state.counter] || backgroundImage1}>
            <View style={styles.mainPageContainerStyle}>
                <View style={styles.mainPageItemsContainerStyle} >
                    <View style={styles.mainPageLogoContainerStyle} >
                        <Image style={styles.mainPageLogoStyle} source={kuarkLogo} />
                    </View>

                    <View style={styles.mainPageBrandContainerStyle} >
                        <Text style={{ color: colors.white, fontSize: 42, fontFamily: fonts.msRegular}}>Kuark</Text>
                        <Text style={{ color: colors.blueKuark, fontSize: 42, fontFamily: fonts.msRegular}}>Tek</Text>
                    </View>

                    <View style={styles.mainPageInformationContainerStyle} >
                        <Text style={styles.mainPageInformationTextStyle}>
                            KuarkTek, kurumsal internet çözümleri sunan, Ankara merkezli yaratıcı bir bilişim ajansıdır.
                        </Text>
                    </View>

                    <TouchableOpacity activeOpacity={0.6} onPress={() => this.props.navigation.navigate('MenuPage')} >
                        <View style={styles.mainPageButtonContainerStyle} >
                            <Text style={styles.mainPageButtonTextStyle}>Buradan Buyrun</Text>
                        </View>
                    </TouchableOpacity>
                </View>

                <View style={styles.mainPageSocialMediaContainerStyle} >
                    <TouchableOpacity activeOpacity={0.6} onPress={handlePressFacebook} >
                        <View style={styles.mainPageSocialMediaIconContainerStyle}>
                            <Icon name="facebook" size={24} color="#fff" />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.6} onPress={handlePressTwitter} >
                        <View style={styles.mainPageSocialMediaIconContainerStyle}>
                            <Icon name="twitter" size={24} color="#fff" />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.6} onPress={handlePressInstagram} >
                        <View style={styles.mainPageSocialMediaIconContainerStyle}>
                            <Icon name="instagram" size={24} color="#fff" />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
            </ImageBackground>
        );
    }
}
export default MainPage;