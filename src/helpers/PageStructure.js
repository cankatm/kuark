import React, { Component } from 'react';
import { StyleSheet, Text, View, Animated, Easing, StatusBar } from 'react-native';
import { StackNavigator, TabNavigator } from 'react-navigation';

import * as colors from './CollorPalette';
import { 
  OpeningScreen, 
  MainPage, 
  MenuPage, 
  BizKimiz,
  NeYapiyoruz,
  NeYaptik, 
  PreviousWorksPhotoShowPage, 
  BizeUlasin, 
} from '../pages';

import { MainHeader } from '../components/Headers';

export const MainNavigator = StackNavigator({
    OpeningScreen: { screen: OpeningScreen },
    MainPage: { screen: MainPage },
    MenuPage: { screen: MenuPage },
    BizKimiz: { screen: BizKimiz },
    NeYapiyoruz: { screen: NeYapiyoruz },
    NeYaptik: { screen: NeYaptik },
    PreviousWorksPhotoShowPage: { screen: PreviousWorksPhotoShowPage },
    BizeUlasin: { screen: BizeUlasin },
  },
  {
    headerMode: 'none',
    lazyLoad: true,
    navigationOptions: {
      gesturesEnabled: false
    }
  }
);