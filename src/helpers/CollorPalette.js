export const blueKuark = '#4E76BC';
export const darkBlueKuark = '#191B1D';
export const white = '#fff';
export const darkestGrey = '#333';
export const lightGrey = '#e5e5e5';
export const lightestGrey = '#f5f5f5';
export const black = '#000';