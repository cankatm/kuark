import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    ImageBackground,
    TouchableOpacity
} from 'react-native';
import { withNavigation } from 'react-navigation';

import * as colors from '../../helpers/CollorPalette';
import styles from './styles';

class ImageShowArea extends Component {
    render() {
        return (
            <ImageBackground source={this.props.imageSource} style={styles.imageShowAreaImageContainerStyle} imageStyle={{ backgroundColor: colors.darkBlueKuark }} >
                <TouchableOpacity 
                    activeOpacity={0} 
                    onPress={() => this.props.navigation.navigate('PreviousWorksPhotoShowPage', { imageSource: this.props.imageSource, workName: this.props.workName })} 
                >
                    <View style={styles.imageShowAreaContainerStyle} >
                        <Text style={styles.imageShowAreaTextStyle}>{this.props.workName}</Text>
                    </View>
                </TouchableOpacity>
            </ImageBackground>
        );
    }
}

export default withNavigation(ImageShowArea);