import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';

import * as colors from '../../helpers/CollorPalette';
import * as fonts from '../../helpers/fonts';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

export default StyleSheet.create({
    imageShowAreaImageContainerStyle: {
        width: WINDOW_WIDTH / 2,
        height: WINDOW_WIDTH / 2,
    },
    imageShowAreaContainerStyle: {
        width: WINDOW_WIDTH / 2,
        height: WINDOW_WIDTH / 2,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.5)'
    },
    imageShowAreaTextStyle: {
        textAlign: 'center',
        color: colors.white,
        fontSize: 12,
        paddingHorizontal: 8, 
        fontFamily: fonts.msRegular
    },
});