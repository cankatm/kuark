import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    TouchableOpacity,
    Image
} from 'react-native';
import { withNavigation } from 'react-navigation';

import styles from './styles';
import { blueLine } from '../../data/data';

class BackHeader extends Component {
    render() {
        return (
            <View style={styles.backHeaderContainerStyle} >
                <TouchableOpacity activeOpacity={0.6} onPress={() => this.props.navigation.goBack()} >
                    <View style={styles.backHeaderButtonContainerStyle}>
                        <Text style={styles.backHeaderButtonXStyle} >X</Text>
                    </View>
                </TouchableOpacity>
                <View style={{ flexDirection: 'row', marginTop: 16 }} >
                    <Text style={styles.backHeaderTitleFirstStyle}>{this.props.titleFirst}</Text>
                    <Text style={styles.backHeaderTitleSecondStyle}>{this.props.titleSecond}</Text>
                </View>
                <Image source={blueLine} style={styles.backHeaderLineImageStyle} />
            </View>
        );
    }
}

export default withNavigation(BackHeader);