import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';

import * as colors from '../../helpers/CollorPalette';
import * as fonts from '../../helpers/fonts';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

export default StyleSheet.create({
    backHeaderContainerStyle: {
        width: WINDOW_WIDTH,
        height: 170,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.darkBlueKuark
    },
    backHeaderButtonContainerStyle: {
        width: 40,
        height: 100,
        backgroundColor: colors.blueKuark,
        alignItems: 'center',
        justifyContent: 'center'
    },
    backHeaderButtonXStyle: {
        color: colors.white,
        fontSize: 24
    },
    backHeaderTitleFirstStyle: {
        color: colors.white,
        fontSize: 24,
        fontFamily: fonts.msRegular
    },
    backHeaderTitleSecondStyle: {
        color: colors.blueKuark,
        fontSize: 24,
        marginLeft: 4,
        fontFamily: fonts.msRegular
    },
    backHeaderLineImageStyle: { 
        width: 80, 
        height: 2, 
        resizeMode: 'contain', 
        marginTop: 8, 
        marginBottom: 16 
    },
});