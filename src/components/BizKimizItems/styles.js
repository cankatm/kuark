import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';

import * as colors from '../../helpers/CollorPalette';
import * as fonts from '../../helpers/fonts';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

export default StyleSheet.create({
    bizKimizContainerStyle: {
        width: WINDOW_WIDTH - 64,
        marginTop: 24
    },
    bizKimizTitleTextStyle: {
        fontSize: 16,
        fontFamily: fonts.msRegular
    },
    bizKimizTitleContainerStyle: {
        width: WINDOW_WIDTH - 64, 
        alignItems: 'center', 
        justifyContent: 'center' 
    },
    bizKimizContentTextStyle: {
        fontSize: 12, 
        fontFamily: fonts.msRegular
    },
    bizKimizLineImageStyle: { 
        width: 80, 
        height: 2, 
        resizeMode: 'contain', 
        marginTop: 8, 
        marginBottom: 16 
    },
});