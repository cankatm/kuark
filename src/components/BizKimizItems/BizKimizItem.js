import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    Image 
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import * as colors from '../../helpers/CollorPalette';
import styles from './styles';
import { blueLine } from '../../data/data';

class BizKimizItem extends Component {
    render() {
        const { title, description } = this.props;
        return (
            <View style={styles.bizKimizContainerStyle} >
                <View style={styles.bizKimizTitleContainerStyle} >
                    <Text style={styles.bizKimizTitleTextStyle} >{title}</Text>
                    <Image source={blueLine} style={styles.bizKimizLineImageStyle} />
                </View>
                <Text style={styles.bizKimizContentTextStyle} >{description}</Text>
            </View>
        );
    }
}

export default BizKimizItem;