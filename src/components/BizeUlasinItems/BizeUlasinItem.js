import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    Animated,
    Image,
    Linking,
    TouchableOpacity,
    Alert
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import * as colors from '../../helpers/CollorPalette';
import * as fonts from '../../helpers/fonts';
import styles from './styles';
import { blueLine } from '../../data/data';

class BizeUlasinItem extends Component {
    constructor(props) {
        super(props)
        this.state = {
            animation: new Animated.Value(1)
        }
    }

    componentWillMount() {
        this.setState({ user: { photoURL: 'https://facebook.github.io/react-native/docs/assets/favicon.png' }});
        this.handleAnimation();
    }

    handleAnimation = () => {
        Animated.timing(this.state.animation, {
            toValue: 1.1,
            duration: 700
        }).start(({finished}) => {
            Animated.timing(this.state.animation, {
                toValue: 1,
                duration: 700
            }).start(() => {
                this.handleAnimation();
            })
        })
    }

    render() {
        const animatedStyle = {
            transform: [
                {
                    scale: this.state.animation
                }
            ]
        }
        
        const { 
            communication, 
            onPress, 
            url, 
            iconName, 
            title, 
            contentFirst, 
            contentSecond 
        } = this.props;

        const renderAlert = () => {
            Alert.alert(
                `${title}`,
                `Uygulamadan ayrılmak üzeresiniz, kabul ediyorsanız Devam\'a basın.`,
                [
                    {text: 'İptal', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {text: 'Devam', onPress: renderLink},
                ],
                { cancelable: true }
            );
        }

        const renderLink = () => {
            if(!communication) {
                return onPress();
            }
            return Linking.openURL(url);
        }

        return (
            <TouchableOpacity onPress={renderAlert} >
                <View style={styles.bizeUlasinContainerStyle}>
                    <Animated.View style={[{width: 100, height: 100, alignItems: 'center', justifyContent: 'center' }, animatedStyle]}>
                        <Icon name={iconName} size={64} color={colors.darkBlueKuark} />
                    </Animated.View>
                    <Image source={blueLine} style={styles.bizeUlasinLineImageStyle} />
                    <Text style={{ fontSize: 24, color: colors.blueKuark, fontFamily: fonts.msRegular }} >{title}</Text>
                    <View style={{ width: 240, alignItems: 'center', justifyContent: 'center', marginTop: 16 }} >
                        <Text style={{ textAlign: 'center', fontSize: 14, color: colors.darkBlueKuark, fontFamily: fonts.msRegular }} >{contentFirst}</Text>
                        <Text style={{ textAlign: 'center', fontSize: 14, color: colors.darkBlueKuark, fontFamily: fonts.msRegular }} >{contentSecond}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

export default BizeUlasinItem;