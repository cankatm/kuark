import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';

import * as colors from '../../helpers/CollorPalette';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

export default StyleSheet.create({
    bizeUlasinContainerStyle: {
        width: WINDOW_WIDTH,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 24
    },
    bizeUlasinLineImageStyle: { 
        width: 80, 
        height: 2, 
        resizeMode: 'contain', 
        marginTop: 8, 
        marginBottom: 16 
    },
});