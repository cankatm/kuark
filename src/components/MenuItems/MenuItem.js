import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    ImageBackground,
    TouchableOpacity
} from 'react-native';
import { withNavigation } from 'react-navigation';

import styles from './styles';

class MenuItem extends Component {
    render() {
        return (
            <ImageBackground style={styles.menuItemBackgroundImageStyle} source={this.props.bgImage}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate(this.props.navigationTo)} activeOpacity={0} >
                    <View style={styles.menuItemContainerStyle} >
                        <Text style={styles.menuItemTextStyle}>{this.props.title}</Text>
                    </View>
                </TouchableOpacity>
            </ImageBackground>
        );
    }
}

export default withNavigation(MenuItem);