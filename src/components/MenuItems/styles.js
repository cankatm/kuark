import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';

import * as colors from '../../helpers/CollorPalette';
import * as fonts from '../../helpers/fonts';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

export default StyleSheet.create({
    menuItemBackgroundImageStyle: {
        width: WINDOW_WIDTH,
        height: WINDOW_HEIGHT / 4,
    },
    menuItemContainerStyle: {
        width: WINDOW_WIDTH,
        height: WINDOW_HEIGHT / 4,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.4)'
    },
    menuItemTextStyle: {
        color: colors.white,
        fontSize: 24, 
        fontFamily: fonts.msRegular
    },
});