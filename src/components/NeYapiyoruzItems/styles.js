import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';

import * as colors from '../../helpers/CollorPalette';
import * as fonts from '../../helpers/fonts';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

export default StyleSheet.create({
    neYapiyoruzButtonContainerStyle: {
        width: WINDOW_WIDTH,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderColor: colors.lightestGrey,
        backgroundColor: colors.white
    },
    neYapiyoruzButtonIconAndTextContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    neYapiyoruzButtonIconContainerStyle: {
        width: 48,
        height: 48,
        marginVertical: 16,
        marginLeft: 12,
        alignItems: 'center',
        justifyContent: 'center'
    },
    neYapiyoruzButtonTextStyle: {
        color: colors.blueKuark,
        fontSize: 14, 
        fontFamily: fonts.msRegular
    },
});