import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    TouchableOpacity,
    Modal,
    Dimensions,
    Image
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import * as colors from '../../helpers/CollorPalette';
import * as fonts from '../../helpers/fonts';

import { kuarkLogoDark, blueLine } from '../../data/data';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

import styles from './styles';

class NeYapiyoruzButton extends Component {
    state = {
      modalVisible: false,
    };
  
    setModalVisible(visible) {
      this.setState({modalVisible: visible});
    }
    
    render() {
        const { iconName, title, description } = this.props;
        return (
            <View>
                <TouchableOpacity onPress={() => this.setModalVisible(true)} >
                    <View style={styles.neYapiyoruzButtonContainerStyle} >
                        <View style={styles.neYapiyoruzButtonIconAndTextContainerStyle} >
                            <View style={styles.neYapiyoruzButtonIconContainerStyle}>
                                <Icon name={iconName} size={24} color={colors.lightGrey} />
                            </View>
                            <Text style={styles.neYapiyoruzButtonTextStyle}>{title}</Text>
                        </View>
                        <View style={styles.neYapiyoruzButtonIconContainerStyle}>
                            <Icon name="arrow-right" size={20} color={colors.darkBlueKuark} />
                        </View>
                    </View>
                </TouchableOpacity>
                        
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                      console.log('Modal has been closed.')
                    }}
                >
                    <View style={{
                        position: 'absolute', 
                        bottom: 0, 
                        left: 0, 
                        width: WINDOW_WIDTH, 
                        height: WINDOW_HEIGHT, 
                        alignItems: 'center',
                        backgroundColor: colors.white
                    }}>
                        <View style={{ width: WINDOW_WIDTH - 64, marginTop: 64, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'  }} >
                            <Text style={{ textAlign: 'center', color: colors.darkBlueKuark, fontSize: 18, fontFamily: fonts.msRegular }} >{title}</Text>
                        </View>

                        <Image source={blueLine} style={{ 
                            width: 80, 
                            height: 2, 
                            resizeMode: 'contain', 
                            marginTop: 8, 
                            marginBottom: 16 
                        }} />

                        <View style={{ width: WINDOW_WIDTH - 64, marginTop: 24, zIndex: 12 }} >
                            <Text style={{ textAlign: 'center', color: colors.darkBlueKuark, fontSize: 12, fontFamily: fonts.msRegular }} >{description}</Text>
                        </View>

                        <View style={{ zIndex: 12 }} >
                            <TouchableOpacity
                                onPress={() => {
                                this.setModalVisible(!this.state.modalVisible);
                                }}
                            >
                                <View style={{ width: 32, height: 32, alignItems: 'center', justifyContent: 'center', marginTop: 16 }} >
                                    <Icon name="arrow-down" size={24} color={colors.blueKuark} />
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{ position: 'absolute', right: 0, bottom: 0, opacity: 0.2, alignItems: 'center', justifyContent: 'center' }} >
                            <Icon name={iconName} size={120} color={colors.lightGrey} />
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
}

export default NeYapiyoruzButton;