import imageSrc1 from '../../assets/images/1.jpg';
import imageSrc2 from '../../assets/images/2.jpg';
import imageSrc3 from '../../assets/images/3.jpg';
import imageSrc4 from '../../assets/images/4.jpg';
import imageSrc5 from '../../assets/images/5.jpg';
import imageSrc6 from '../../assets/images/6.jpg';
import imageSrc7 from '../../assets/images/7.jpg';
import imageSrc8 from '../../assets/images/8.jpg';
import imageSrc9 from '../../assets/images/9.jpg';
import imageSrc10 from '../../assets/images/10.jpg';
import imageSrc11 from '../../assets/images/11.jpg';
import imageSrc12 from '../../assets/images/12.jpg';
import imageSrc13 from '../../assets/images/13.jpg';
import imageSrc14 from '../../assets/images/14.jpg';
import imageSrc15 from '../../assets/images/15.jpg';
import imageSrc16 from '../../assets/images/16.jpg';
import imageSrc17 from '../../assets/images/17.jpg';
import imageSrc18 from '../../assets/images/18.jpg';
import imageSrc19 from '../../assets/images/19.jpg';
import imageSrc20 from '../../assets/images/20.jpg';
import imageSrc21 from '../../assets/images/21.jpg';
import imageSrc22 from '../../assets/images/22.jpg';
import imageSrc23 from '../../assets/images/23.jpg';
import imageSrc24 from '../../assets/images/24.jpg';
import imageSrc25 from '../../assets/images/25.jpg';
import imageSrc26 from '../../assets/images/26.jpg';
import imageSrc27 from '../../assets/images/27.jpg';
import imageSrc28 from '../../assets/images/28.jpg';
import imageSrc29 from '../../assets/images/29.jpg';
import imageSrc30 from '../../assets/images/30.jpg';
import imageSrc31 from '../../assets/images/31.jpg';
import imageSrc32 from '../../assets/images/32.jpg';
import imageSrc33 from '../../assets/images/33.jpg';
import imageSrc34 from '../../assets/images/34.jpg';
import imageSrc35 from '../../assets/images/35.jpg';
import imageSrc36 from '../../assets/images/36.jpg';

export const kuarkLogo = require('../../assets/images/kuarkLogo.png');
export const kuarkLogoDark = require('../../assets/images/kuarkLogoDark.png');
export const blueLine = require('../../assets/images/altcizgi.jpg');

export const bizKimiz = [
    { 
        id: 1, 
        title: 'EKİBİMİZ', 
        description: `KuarkTek, bilime olduğu kadar ideallerine de inanan bir grup genç mühendis ve yazılımcının bir araya gelmesiyle doğan, her zaman en iyiyi hedefleyen, inovatif düşünceyi benimseyen, tasarım ve ar-ge’den büyük keyif alan profesyonel bir ekiptir.\n\nKuarkTek, markalaşma ve kurumsallaşma sürecinin her adımında müşterilerinin yanında bulunarak onlar ile uzun soluklu çözüm ortaklıkları kurmayı hedefler. İhtiyaçların tespiti konusunda müşterileri ile birlikte yol alır. Uzun soluklu, kalıcı çözüm önerileri sunar.\n\nBilimle sanatın iç içe geçtiği, eğlence ve işin bir arada gerçekleştiği dinamik çalışma ortamı, yetenekli, zeki ve girişimci ekibimizle hizmetinizdeyiz...` 
    },
    { 
        id: 2, 
        title: 'KUARK NEDİR?', 
        description: `Kuark temel parçacık ve maddenin temel bileşenlerinden biridir. Kuarklar bir araya gelerek hadronlar olarak bilinen bileşik parçacıkları oluştururlar. Bunların en kararlı olanları atom çekirdeğinin bileşenleri proton ve nötrondur.\n\nRenk hapsi denilen bir olgu sebebiyle kuarklar asla yalnız bir şekilde bulunmazlar; onlar sadece hadronlar dahilinde bulunabilirler. Bu sebeple kuarklar hakkında bilinenlerin çoğu hadronların gözlenmesi sonucunda elde edilmiştir.\n\nÇeşni olarak bilinen altı tip kuark bulunmaktadır: yukarı, aşağı, tılsım, acayip, üst ve alt. Yukarı ve aşağı kuark bütün kuarklar içinde en düşük kütleli olanlardır. Daha ağır kuarklar parçacık bozunması yoluyla hızlıca aşağı ve yukarı kuarka dönüşürler. Bu sebeple yukarı ve aşağı kuarklar evrende en yaygın olanlardır, bununla birlikte tılsım, acayip, üst ve alt kuarklar sadece yüksek enerjili çarpışmalarda (kozmik ışınlar ve parçacık hızlandırıcılarda) oluşabilir.\n\nKuarklar elektrik yükü, renk yükü, spin ve kütle gibi çeşitli içkin özelliklere sahiptir. Kuarklar parçacık fiziğinin Standart Model'inde dört temel kuvvetin (elektromanyetizma, gravitasyon, güçlü etkileşim, zayıf etkileşim) tümüyle de etkileşen ve ayrıca elektrik yükü temel yükün tamsayı katı olmayan tek temel tanecik ailesidir. Her kuark çeşnisi için ona karşılık gelen bir tane de antiparçacık bulunur. Antikuark denilen bu parçacık kuarktan, sadece bazı özelliklerinin aynı büyüklükte fakat ters işaretli olması ile ayrılır.` 
    },
    { 
        id: 3, 
        title: 'UYGULAMA HAKKINDA', 
        description: `Bu uygulama KuarkTek - SinsApp yapımıdır. Tasarım ve geliştirme, Emre Yılmaz ve Mert Cankat tarafından yapılmıştır.` 
    },
];

export const images = [
    { imageSource: imageSrc1, workName: 'Göksun Mekanik' },
    { imageSource: imageSrc2, workName: 'PLY Mühendislik' },
    { imageSource: imageSrc3, workName: 'Günce Grup Sigorta' },
    { imageSource: imageSrc4, workName: 'Volo Savunma' },
    { imageSource: imageSrc5, workName: 'Op. Dr. Tevfik Sipahi' },
    { imageSource: imageSrc6, workName: 'Gülhan Mimarlık' },
    { imageSource: imageSrc7, workName: 'Prof. Dr. Sedat Köse' },
    { imageSource: imageSrc8, workName: 'ASELSAN' },
    { imageSource: imageSrc9, workName: "MEB - AB\'yi Öğren Projesi" },
    { imageSource: imageSrc10, workName: 'Decorillo' },
    { imageSource: imageSrc11, workName: 'Ankara SEO Web Tasarım' },
    { imageSource: imageSrc12, workName: 'Fiziform Sağlıklı Yaşam Merkezi' },
    { imageSource: imageSrc13, workName: 'Toraman Makina' },
    { imageSource: imageSrc14, workName: 'Aker İnşaat' },
    { imageSource: imageSrc15, workName: 'Postür Akademi' },
    { imageSource: imageSrc16, workName: 'SilkPeel Mucizesi' },
    { imageSource: imageSrc17, workName: 'Tüp Bebek Türkiye' },
    { imageSource: imageSrc18, workName: 'QCTURK Mühendislik' },
    { imageSource: imageSrc19, workName: '6:45 Kaybedenler Kulübü' },
    { imageSource: imageSrc20, workName: 'Simeti Cnc' },
    { imageSource: imageSrc21, workName: 'Proked Akademi' },
    { imageSource: imageSrc22, workName: 'Sigortacı Kedi' },
    { imageSource: imageSrc23, workName: 'Ot Grup' },
    { imageSource: imageSrc24, workName: 'Mehmet Auf' },
    { imageSource: imageSrc25, workName: 'Karadeniz Omurilik Felçlileri Derneği' },
    { imageSource: imageSrc26, workName: 'Kerem Aygün Koleji' },
    { imageSource: imageSrc27, workName: 'Karar Denetim' },
    { imageSource: imageSrc28, workName: 'Giresun Yatırım' },
    { imageSource: imageSrc29, workName: 'Fuckupnights Ankara' },
    { imageSource: imageSrc30, workName: 'Ankara Karo' },
    { imageSource: imageSrc31, workName: 'Mahmut Kömürcü' },
    { imageSource: imageSrc32, workName: 'KuarkTek Mobil Uygulama' },
    { imageSource: imageSrc33, workName: '6:45 Kaybedenler Kulübü Mobil Uygulama' },
    { imageSource: imageSrc34, workName: '6:45 Kaybedenler Kulübü Menü' },
    { imageSource: imageSrc35, workName: 'Simetri Cnc Katalog' },
    { imageSource: imageSrc36, workName: 'Volo Savunma Katalog' },
];

export const neYapiyoruzItems = [
    { 
        id: 1, 
        iconName: 'cellphone-iphone', 
        title: 'KURUMSAL WEB TASARIMI', 
        description: 'Ürün, Marka ve Hizmetiniz ne olursa olsun, profesyonel ve proje odaklı tasarımlarımızla sizlere prestij ve kar kazandırmak için çaışıyoruz. Üstün Mobil uyum ve cihaza özel tasarımlar ile internet sitelerizine hareket ve değer kazandırıyoruz.' 
    },
    { 
        id: 2, 
        iconName: 'earth', 
        title: 'ARAMA MOTORU OPTİMİZASYONU', 
        description: 'Ürün ve hedef pazarlarınızı inceleyerek yaptığımız anahtar kelime analizleri sonucunda, projeniz için en etkili arama hedeflerini belirliyoruz. Sektörünüze yönelik aramalar yapan ve henüz tanışmadığınız hedef kitle ile web sitenizi buluşturuyoruz.' 
    },
    { 
        id: 3, 
        iconName: 'tune', 
        title: 'KULLANICI DENEYİMİ TASARIMI (UX)', 
        description: 'Ürün ve hizmetiniz ne olursa olsun, web sitelerinizde son kullanıcınıza iyi bir deneyim sunmanızı sağlıyoruz.' 
    },
    { 
        id: 4, 
        iconName: 'vector-circle', 
        title: 'ÖZEL YAZILIM GELİŞTİRME', 
        description: 'Proje bazlı geliştirdiğimiz yazılımlarla doğru tanımlanmış problemleri etkili bir şekilde çözmenizi sağlıyoruz.' 
    },
    { 
        id: 5, 
        iconName: 'thought-bubble-outline', 
        title: 'SOSYAL MEDYA VE PRESTİJ YÖNETİMİ', 
        description: 'Markanızın tüm sosyal platformlarda başarılı şekilde tanıtılmasını ve yönetilmesini sağlıyoruz.' 
    },
    { 
        id: 6, 
        iconName: 'trophy-outline',  
        title: 'KREATİF REKLAMCILIK', 
        description: 'Yazılı ve görsel medyada yayınlanmak üzere grafik tasarımı, fotoğraf çekimi, spot film ve tanıtım filmi gibi kaliteli ve yaratıcı tasarım ve prodüksiyon çözümleri sunuyoruz.' 
    },
];

export const googleAddress = 'https://www.google.com.tr/maps/dir//Mebusevleri+Mahallesi,+KuarkTek,+Re%C5%9Fit+Galip+Cd.+No:45,+06680+%C3%87ankaya%2FAnkara/@39.8991352,32.8683949,17z/data=!4m15!1m6!3m5!1s0x14d34fa8a0dc56b5:0xb9f837e6dea8b317!2sKuarkTek!8m2!3d39.899246!4d32.869696!4m7!1m0!1m5!1m1!1s0x14d34fa8a0dc56b5:0xb9f837e6dea8b317!2m2!1d32.869696!2d39.899246';
export const phoneNumber = '+903124177793';
export const emailAddress = 'info@kuarktek.com.tr';