import React from 'react';
import { 
  StyleSheet, 
  Text, 
  View,
  StatusBar
} from 'react-native';
import { StackNavigator, TabNavigator } from 'react-navigation';
import { Provider } from 'react-redux';
import { Font } from 'expo';

import * as data from './src/data/data';
import store from './src/store';
import { MainNavigator } from './src/helpers/PageStructure';

import * as colors from './src/helpers/CollorPalette';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fontLoaded: false
    }
  }

  componentDidMount() {
    Font.loadAsync({
      'montserratRegular': require('./assets/fonts/Montserrat-Regular.ttf'),
      'montserratLight': require('./assets/fonts/Montserrat-Light.ttf'),
    })
    .then(() => {
      this.setState({ fontLoaded: true });
    });
  }

  render() {
    const renderApp = () => {
      if (!this.state.fontLoaded) {
        return <Expo.AppLoading />;
      } else {
        return <MainNavigator />;
      }
    }

    return (
      <Provider store={store}>
        <View style={styles.container} >
          <StatusBar hidden />
          {renderApp()}
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
});
